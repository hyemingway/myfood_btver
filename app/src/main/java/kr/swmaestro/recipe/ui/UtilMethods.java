package kr.swmaestro.recipe.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.db.chart.Tools;
import com.db.chart.model.BarSet;
import com.db.chart.view.AxisController;
import com.db.chart.view.ChartView;
import com.db.chart.view.HorizontalStackBarChartView;
import com.db.chart.view.animation.Animation;
import com.db.chart.view.animation.easing.QuartEase;

import kr.swmaestro.recipe.R;
import kr.swmaestro.recipe.helper.SQLiteManager;
import kr.swmaestro.recipe.model.Nutrition;
import kr.swmaestro.recipe.model.Recipe;
import kr.swmaestro.recipe.model.SQLSupport;

/**
 * Created by Kahye on 2015. 12. 3..
 */

public class UtilMethods extends DemoBase  {
    private static AlertDialog dialog = null;
//    protected static HorizontalBarChart mChart;
//    private static SeekBar mSeekBarX, mSeekBarY;
//    private static TextView tvX, tvY;


    private static BarSet dataset;
    private static Paint gridPaint;
    private static Animation anim;
    private static HorizontalStackBarChartView chart;

    private static SQLiteManager dbManager;


    //default dataset
    private String[] labels = { "나트륨", "지질", "단백질", "탄수화물"};
    private float[] values = {0, 30, 50, 100};


    public static void showGraghDialog(final Context context, final String heading, final String body,
                                       final String positiveString, final String negativeString, final Recipe check_recipe) {

        final Recipe clickedRecipe = check_recipe;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_dialog, null);

        dbManager = new SQLiteManager(context, "food.db", null, 1);
        dbManager.createTable(SQLSupport.EATEN_FOOD_TABLE);

        Nutrition avgNut = dbManager.getAvgNutrition(SQLSupport.EATEN_FOOD_TABLE);

        String[] labels = { "나트륨", "지질", "단백질", "탄수화물"};
       
        float[] values = {avgNut.getSodium(), avgNut.getFat(),
                avgNut.getProtein(), avgNut.getCarbo()};

        dataset = new BarSet(labels, values);
        dataset.setColor(Color.parseColor("#e47400"));

        int count = dbManager.getRowCount(SQLSupport.EATEN_FOOD_TABLE);
        if(count == 0){
            count = 1;
        }
        Log.d("UtilMethod", "count is " + count);
        float[] values2 = {clickedRecipe.getSodium()/count,clickedRecipe.getFat()/count,
                clickedRecipe.getProtein()/count, clickedRecipe.getCarbo()/count};

        BarSet dataset2 = new BarSet(labels, values2);

        chart = (HorizontalStackBarChartView) view.findViewById(R.id.stackChart);
        chart.setBarSpacing(32.0f);
        chart.setBarBackgroundColor(Color.parseColor("#99727272"));
        chart.addData(dataset);
        dataset2.setColor(Color.parseColor("#FFD700"));
        chart.addData(dataset2);

// Generic chart customization
        chart.setX(0);
// Paint object used to draw Grid
        gridPaint = new Paint();
        gridPaint.setColor(Color.parseColor("#727272"));
        gridPaint.setStyle(Paint.Style.STROKE);
        gridPaint.setAntiAlias(true);
        gridPaint.setStrokeWidth(Tools.fromDpToPx(1.0f));
        chart.setGrid(ChartView.GridType.NONE, gridPaint);
//        chart.setLabelsFormat(new DecimalFormat("##"));

// Animation customization
        anim = new Animation(500);
        anim.setEasing(new QuartEase());
        anim.setStartPoint(0.0f, -1.0f);
//        chart.setXLabels(AxisController.LabelPosition.NONE);
        chart.show(anim);

        ((TextView) view.findViewById(R.id.headlineTV)).setText(heading);
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setPositiveButton(positiveString, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO : 이 음식을 먹는다.

                        dbManager.insert(SQLSupport.EATEN_FOOD_TABLE, clickedRecipe.getTitle(), clickedRecipe.getCarbo(),
                                clickedRecipe.getProtein(), clickedRecipe.getFat(),
                                clickedRecipe.getSodium(), clickedRecipe.getSodium());
//                        Log.d("UtilMethod", dbManager.PrintData(SQLSupport.EATEN_FOOD_TABLE));
                        Log.d("UtilMethod", "음식 먹은 후 rowcount" + dbManager.getRowCount(SQLSupport.EATEN_FOOD_TABLE) +"");
                        dialog.cancel();
                    }
                })
                .setNegativeButton(negativeString, new DialogInterface.OnClickListener() {//yes
                    public void onClick(DialogInterface dialog, int which) {
//
                        //TODO : 아무일도 하지 않는다.

                        dialog.cancel();
                    }
                })
                .setView(view)
                .setCancelable(true);


        dialog = builder.create();
        dialog.show();
    }



}
