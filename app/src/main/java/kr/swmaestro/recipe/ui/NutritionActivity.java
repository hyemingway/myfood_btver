package kr.swmaestro.recipe.ui;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.db.chart.Tools;
import com.db.chart.model.BarSet;
import com.db.chart.view.AxisController;
import com.db.chart.view.ChartView;
import com.db.chart.view.HorizontalBarChartView;
import com.db.chart.view.animation.Animation;
import com.db.chart.view.animation.easing.QuartEase;

import java.text.DecimalFormat;

import kr.swmaestro.recipe.R;
import kr.swmaestro.recipe.helper.SQLiteManager;
import kr.swmaestro.recipe.model.Nutrition;
import kr.swmaestro.recipe.model.SQLSupport;

public class NutritionActivity extends AppCompatActivity {
    private BarSet dataset;
    private Paint gridPaint;
    private Animation anim;
    private HorizontalBarChartView chart;

    //default dataset
    private String[] labels = { "나트륨", "지질", "단백질", "탄수화물"};
    private float[] values = {0,0,0,10 };

    SQLiteManager sqlManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition);


        setData();
        setUI();


    }

    private void setData() {
        //새로운 Barset을 만든다.
        sqlManager = new SQLiteManager(getApplicationContext(),"food.db",null,1);
        // sqlManager.dropTable(SQLSupport.EATEN_FOOD_TABLE);
        sqlManager.createTable(SQLSupport.EATEN_FOOD_TABLE);

//        sqlManager.insert(SQLSupport.EATEN_FOOD_TABLE, "foodname", 2, 4, 6, 8, 14);
        Log.d("SQLite Check", sqlManager.PrintData(SQLSupport.EATEN_FOOD_TABLE));
    }

    private void setUI() {

        dataset = new BarSet(labels, values);
        dataset.setColor(Color.parseColor("#e47400"));


        chart = (HorizontalBarChartView) this.findViewById(R.id.linechart);

        chart.setBarSpacing(32.0f);
        chart.setBarBackgroundColor(Color.parseColor("#99727272"));
        chart.addData(dataset);

// Generic chart customization
// Paint object used to draw Grid
        gridPaint = new Paint();
        gridPaint.setColor(Color.parseColor("#727272"));
        gridPaint.setStyle(Paint.Style.STROKE);
        gridPaint.setAntiAlias(true);
        gridPaint.setStrokeWidth(Tools.fromDpToPx(1.0f));
        chart.setGrid(ChartView.GridType.NONE, gridPaint);
//        chart.setLabelsFormat(new DecimalFormat("##"));

// Animation customization
        anim = new Animation(500);
        anim.setEasing(new QuartEase());
        anim.setStartPoint(0.0f, -1.0f);
//        chart.show(anim);

        change();


    }

    public void change(){
//        sqlManager.insert(SQLSupport.EATEN_FOOD_TABLE, "foodname2", 5, 2, 6, 7, 19);
        Log.d("SQLite Check", sqlManager.PrintData(SQLSupport.EATEN_FOOD_TABLE));

        Nutrition avg_nutri = sqlManager.getAvgNutrition(SQLSupport.EATEN_FOOD_TABLE);
        values[0] = avg_nutri.getSodium();
        values[1] = avg_nutri.getFat();
        values[2] = avg_nutri.getProtein();
        values[3] = avg_nutri.getCarbo();

        dataset.updateValues(values);
        chart.setXLabels(AxisController.LabelPosition.NONE);

        chart.show(anim);


    }

    public void drop(View v){
        sqlManager.dropTable(SQLSupport.EATEN_FOOD_TABLE);
        Toast.makeText(getApplicationContext(),SQLSupport.EATEN_FOOD_TABLE+" table is droped!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nutrition, menu);
        return true;
    }

}
