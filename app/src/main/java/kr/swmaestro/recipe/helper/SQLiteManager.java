package kr.swmaestro.recipe.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import kr.swmaestro.recipe.model.Nutrition;
import kr.swmaestro.recipe.model.SQLSupport;

/**
 * Created by ljs93kr on 2015-12-06.
 */
public class SQLiteManager extends SQLiteOpenHelper {


    public SQLiteManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DBManager", "this is onCreate");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DBManager", "this is onUpdate");
    }

    public void createTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        String _query = "CREATE TABLE IF NOT EXISTS " + tableName + " " + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, carbo INTEGER, protein INTEGER, fat INTEGER, sodium INTEGER, calory INTEGER, time datetime);";
        db.execSQL(_query);
        db.close();
    }

    public void dropTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        String _query = "drop table IF exists " + tableName;
        db.execSQL(_query);
        db.close();
    }

    public void insert(String tableName, String name, int carbo, int protein, int fat, int sodium, int cal) {
        SQLiteDatabase db = getWritableDatabase();
        String _query = "insert into " + tableName + " values(null, '" + name + "'," + carbo + ", " + protein + ", " + fat + ", " + sodium + ", " + cal + ", DATETIME('NOW', 'LOCALTIME' ));";
        db.execSQL(_query);
        db.close();
    }

    public void update(String _query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(_query);
        db.close();
    }

    public void delete(String tableName, String dele, int flag) {
        SQLiteDatabase db = getWritableDatabase();
        String _query;
        switch (flag) {
            case SQLSupport.BY_ID:
                _query = "delete from " + tableName + " where _id = '" + dele + "';";
                break;
            case SQLSupport.BY_NAME:
                _query = "delete from " + tableName + " where name = '" + dele + "';";
                break;
            default:
                db.close();
                return;

        }
        db.execSQL(_query);
        db.close();
    }

    public String PrintData(String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        String str = "";

        try {
            Cursor cursor = db.rawQuery("select * from " + tableName, null);
            while (cursor.moveToNext()) {
                str += cursor.getInt(cursor.getColumnIndex("_id"))
                    + " : name : "
                    + cursor.getString(cursor.getColumnIndex("name"))
                    + " : carbo "
                    + cursor.getInt(cursor.getColumnIndex("carbo"))
                    + ", protein = "
                    + cursor.getInt(cursor.getColumnIndex("protein"))
                    + ", fat = "
                    + cursor.getInt(cursor.getColumnIndex("fat"))
                    + ", sodium = "
                    + cursor.getInt(cursor.getColumnIndex("sodium"))
                    + ", calory = "
                    + cursor.getInt(cursor.getColumnIndex("calory"))
                    + ", time : "
                    + cursor.getString(cursor.getColumnIndex("time"))
                    + "\n";
            }
            db.close();

            return str;
        } catch (Exception e) {
            return "no table";
        }


    }

    public Nutrition getAvgNutrition(String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        int avg_carbo = 0;
        int avg_protein = 0;
        int avg_fat = 0;
        int avg_sodium = 0;
        int count = 0;
        try {
            Cursor cursor = db.rawQuery("select * from " + tableName, null);
            while (cursor.moveToNext()) {
                count++;
                avg_carbo += cursor.getInt(cursor.getColumnIndex("carbo"));
                avg_protein += cursor.getInt(cursor.getColumnIndex("protein"));
                avg_fat += cursor.getInt(cursor.getColumnIndex("fat"));
                avg_sodium += cursor.getInt(cursor.getColumnIndex("sodium"));

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (count <= 0) {
                count = 1;
            }
            avg_carbo = avg_carbo / count;
            avg_protein = avg_protein / count;
            avg_fat = avg_fat / count;
            avg_sodium = avg_sodium / count;
        }

        return new Nutrition(avg_carbo, avg_protein, avg_fat, avg_sodium);
    }

    public Nutrition getTodayNutrition(String tableName) {
        SQLiteDatabase db = getReadableDatabase();

        String q = "SELECT " +
            "  sum(carbo)   AS sum_carbo, " +
            "  sum(protein) AS sum_protein, " +
            "  sum(fat)     AS sum_fat, " +
            "  sum(sodium)  AS sum_sodium " +
            "FROM " + tableName + " " +
            "WHERE time > date('now', 'start of day', 'localtime');";

        int sum_carbo = 0;
        int sum_protein = 0;
        int sum_fat = 0;
        int sum_sodium = 0;

        try {
            Cursor cursor = db.rawQuery(q, null);
            cursor.moveToFirst();

            String log = DatabaseUtils.dumpCursorToString(cursor);
            sum_carbo = cursor.getInt(cursor.getColumnIndex("sum_carbo"));
            sum_protein = cursor.getInt(cursor.getColumnIndex("sum_protein"));
            sum_fat = cursor.getInt(cursor.getColumnIndex("sum_fat"));
            sum_sodium = cursor.getInt(cursor.getColumnIndex("sum_sodium"));

            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Nutrition(sum_carbo, sum_protein, sum_fat, sum_sodium);
    }

    public int getRowCount(String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        int count = 0;
        try {
            Cursor cursor = db.rawQuery("select * from " + tableName, null);
            while (cursor.moveToNext()) {
                count++;
            }
            cursor.close();
        } catch (Exception e) {

        }
        return count;
    }
}
