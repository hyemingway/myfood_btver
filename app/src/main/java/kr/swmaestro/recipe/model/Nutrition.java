package kr.swmaestro.recipe.model;

/**
 * Created by ljs93kr on 2015-12-06.
 */
public class Nutrition {

    private String name;
    private int carbo;
    private int protein;
    private int fat;
    private int sodium;
    private int cal;

    public Nutrition(String name,int carbo, int protein, int fat, int sodium, int cal){
        this.name = name;
        this.carbo = carbo;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.cal = cal;
    }

    public Nutrition(int carbo, int protein, int fat, int sodium){
        this.carbo = carbo;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;

    }



    public String getName() {
        return name;
    }
    public int getCarbo() {
        return carbo;
    }
    public int getProtein() {
        return protein;
    }
    public int getFat() {
        return fat;
    }
    public int getSodium() {
        return sodium;
    }
    public int getCal() {
        return cal;
    }


}
