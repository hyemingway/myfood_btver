package kr.swmaestro.recipe.model;

/**
 * Created by ljs93kr on 2015-12-06.
 */
public class SQLSupport {

    //table name
    public static String EATEN_FOOD_TABLE= "EATEN_FOOD";

    // EATEN_FOOD_TABLE flags
    public static final int BY_ID=1;
    public static final int BY_NAME=2;

}
