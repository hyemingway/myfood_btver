package kr.swmaestro.recipe.model;

/**
 * Created by lk on 2015. 8. 2..
 */
public class Recipe {

    private String title;       // Recipe Title
    private int id;             // Recipe Id
    private String imageUrl;    // Recipe Thumbnail URL
    private String wasLike;     // Like Id

    private int cal;
    private int carbo;
    private int protein;
    private int fat;
    private int sodium;

    public Recipe(String title, String id, String imageUrl, String wasLike) {
        this.title = title;
        this.id = Integer.parseInt(id);
        this.imageUrl = imageUrl;
        this.wasLike = wasLike;
    }

    public void setNutrient(int cal, int carbo, int protein, int fat, int sodium){
        this.cal = cal;
        this.carbo = carbo;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;

    }

    public int getCal(){return cal;}
    public int getCarbo(){return carbo;}
    public int getProtein(){return protein;}
    public int getFat(){return fat;}
    public int getSodium(){return sodium;}

    public String getNutritions(){
        String result;
        result = "carbo : "+getCarbo()+", protein : "+getProtein()+", fat : "+getFat()+", Sodium : "+getSodium();
        return result;
    }

    public String getTitle() { return title; }

    public int getItemId() { return id; }

    public String getImageUrl() { return imageUrl; }

    public String getWasLike(){ return wasLike; }

    public void setWasLike(String id){ this.wasLike = id; }

    @Override
    public String toString() { return id+""; }
}
