package com.soma6th.graphtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button graphDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        graphDialog = (Button) findViewById(R.id.util_graph_dialog);

        graphDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilMethods.showGraghDialog (v.getContext(), "영양소 그래프", "", "NO", "YES");

            }
        });

    }
}
